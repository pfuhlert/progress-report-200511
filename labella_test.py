"""
    Fifth example on:
    https://kristw.github.io/d3kit-timeline/
"""

from datetime import date

from labella.scale import LinearScale
from labella.timeline import TimelineSVG, TimelineTex

def color5(d):
    if d['type'] == 'screening':
        return '#1F77B4'
    elif d['type'] == 'diagnostic':
        return '#c93232'
    elif d['type'] == 'treatment':
        return '#8f46c7'
    elif d['type'] == 'complication':    
        return '#1fb45d'
    else:
        return '#FF7F0E'

def main():
    data = [{"index":41723,"time":115.0,"type":"screening","subtype":"PSA","description":"NG - 0.34"},{"index":41723,"time":115.0,"type":"screening","subtype":"DRE","description":"Abnormal, non-suspicious"},{"index":41724,"time":433.0,"type":"screening","subtype":"PSA","description":"NG - 0.17"},{"index":41724,"time":433.0,"type":"screening","subtype":"DRE","description":"Abnormal, non-suspicious"},{"index":41725,"time":766.0,"type":"screening","subtype":"PSA","description":"NG - 0.55"},{"index":41725,"time":766.0,"type":"screening","subtype":"DRE","description":"Abnormal, non-suspicious"},{"index":41726,"time":1210.0,"type":"screening","subtype":"PSA","description":"NG - 0.44"},{"index":41726,"time":1210.0,"type":"screening","subtype":"DRE","description":"Abnormal, non-suspicious"},{"index":41727,"time":1567.0,"type":"screening","subtype":"PSA","description":"NG - 0.58"},{"index":41728,"time":2036.0,"type":"screening","subtype":"PSA","description":"NG - 0.44"},{"index":23021,"time":3598.0,"type":"diagnostic","subtype":"Imaging","description":"CT scan: Abdomen and Pelvis Combined"},{"index":23020,"time":3598.0,"type":"diagnostic","subtype":"Repeat Screen","description":"DRE Test"},{"index":23022,"time":3601.0,"type":"diagnostic","subtype":"Repeat Screen","description":"DRE Test"},{"index":23024,"time":3622.0,"type":"diagnostic","subtype":"Invasive Scopes, Biopsies, and Surgeries (w/local)","description":"Cystoscopy"},{"index":23023,"time":3622.0,"type":"diagnostic","subtype":"Invasive Scopes, Biopsies, and Surgeries (w/local)","description":"TURP"},{"index":3251,"time":3622.0,"type":"treatment","subtype":"Non-curative treatment","description":"Transurethral resection"},{"index":23025,"time":3622.0,"type":"diagnostic","subtype":"Record Review","description":"Record Review"},{"index":23026,"time":3623.0,"type":"diagnostic","subtype":"Imaging","description":"Ultrasound - abdomen/pelvis"},{"index":3252,"time":3661.0,"type":"treatment","subtype":"Non-curative treatment","description":"Other treatment, NOS"},{"index":23028,"time":3769.0,"type":"diagnostic","subtype":"Invasive Scopes, Biopsies, and Surgeries (w/local)","description":"Lymphadenectomy / Lymph Node Sampling"},{"index":23027,"time":3769.0,"type":"diagnostic","subtype":"Surgeries (w/general)","description":"Prostatectomy"},{"index":3253,"time":3769.0,"type":"treatment","subtype":"Non-curative treatment","description":"Pelvic node dissection (lymphadenectomy), surgical"},{"index":3254,"time":3769.0,"type":"treatment","subtype":"Prostatectomy","description":"Prostatectomy, NOS"},{"index":745,"time":3778.0,"type":"complication","subtype":"Infection","description":"Infection (specify)"},{"index":746,"time":3778.0,"type":"complication","subtype":"Cardiovasculuar/Pulmonary","description":"Hypotension/vasovagal reaction"},{"index":41727,"time":0.0,"type":"screening","subtype":"DRE","description":"Not done, not expected"},{"index":41728,"time":0.0,"type":"screening","subtype":"DRE","description":"Not done, not expected"}]
    
    options = {
        'direction': 'up',
        'initialWidth': 804,
        # 'initialHeight': 120,
        'scale': LinearScale(),
        'xlabel': 'days',
        # 'domain': [0, 1900],
        'margin': {'left': 20, 'right': 20, 'top': 30, 'bottom': 20},
        'textFn': lambda d : d['subtype'],
        'layerGap': 40,
        'dotColor': color5,
        'labelBgColor': color5,
        'linkColor': color5,
        'labella': {
            'maxPos': 764,
            'algorithm': 'simple',
            },
        'labelPadding': {'left': 0, 'right': 2, 'top': 0, 'bottom': 0},
        }

    tl = TimelineSVG(data, options=options)
    tl.export('timeline_kit_5.svg')

    tl = TimelineTex(data, options=options)
    tl.export('timeline_kit_5.tex')

if __name__ == '__main__':
    main()