% !TEX program = xelatex

\documentclass[aspectratio=1610, xcolor={dvipsnames}]{beamer}

\usepackage{amsmath,amsfonts,amsthm,bm}
\usepackage[english]{babel}
\usepackage{siunitx}
\usepackage{transparent}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{tikz}
\usepackage{tikzscale}
\usepackage{pgf-pie}

\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
% \setsansfont[BoldFont={Fira Sans}]{Fira Sans Light}
% \setmonofont{Fira Mono}
\usepackage[sfdefault]{Fira Sans}

\usetikzlibrary{arrows.meta, calc, matrix, positioning, chains, decorations.pathreplacing}

\usetheme{metropolis}
\title{Progress Report}
\date{11.05.2020}
\author{Patrick Fuhlert}
\institute{Institute of Medical Systems Biology}

\metroset{subsectionpage=progressbar, numbering=fraction, titleformat=regular}

\makeatletter

\input{metropolis_extensions.tex}

\begin{document}
  \maketitle
  \begin{frame}{Current Foci}
    \setbeamertemplate{section in toc}[sections numbered]
    \tableofcontents[hideallsubsections]
  \end{frame}  

  \section{Scrum and Agile}

  \begin{frame}{(Remote) Retrospective}
    \vfill
    \begin{figure}[]
      \centering
        \includegraphics[width=.48\textwidth]{img/retrospectives_feature2-1.jpg}
        \includegraphics[width=.48\textwidth]{img/mk-retro1.png}
        \caption*{Five Steps of Retrospectives\footnote{blog.trello.com/the-5-steps-to-better-team-retrospectives} and Martini-Klinik example}
    \end{figure}
  \end{frame}

  \begin{frame}{Agile Day}
    \begin{minipage}{0.45\textwidth}
      \begin{itemize}
        \item Agile Coaching for IMSB Institute
        \item Learn about \textbf{Agile Mindset} and Tools
        \item Project Organization techniques
        \item Meeting structures
        \item Cooperation
        \item \dots
      \end{itemize}
    \end{minipage}
    \hfill
    \begin{minipage}{0.45\textwidth}
      \begin{figure}[]
        \centering
          \includegraphics[width=.9\textwidth]{img/agile-tools}
      \end{figure}
    \end{minipage}
  \end{frame}  

  \section{IT Infrastructure}

  \begin{frame}{Grafana}
    \begin{itemize}
      \item `Grafana is the open source analytics solution for every database'\footnote{grafana.com}
      \item Main Usage: Server Monitoring
      \item Dashboards give nice Visualization
      \item Alerts to avoid server outage (e.g. Memory)
    \end{itemize}
  \end{frame}

  \begin{frame}{Grafana}
    \begin{figure}[]
      \centering
      \includegraphics[width=.9\textwidth]{img/grafana.png}
      \caption*{Sample Grafana Dashboard\footnote{play.grafana.com}}
    \end{figure}
  \end{frame}

  \begin{frame}{Next Steps}
    \begin{itemize}
      \item Pilot Test on PowerEdge
      \item Include Docker Container Observation
      \item Collect data from all IMSB machines 
      \item Enable Alert functionality
    \end{itemize}
  \end{frame}   
  
  \section{Software Development}

  
  \begin{frame}{Project Daisy}
    
    \begin{itemize}
      \item Daisy is a \textbf{data integration} project from Genevention
      \item Used by SEA
      \item Stores Genetic data and images
      \item Could also be used for patient data
      \item IMSB has a running daisy instance
    \end{itemize}
    % \begin{figure}
    %   \includegraphics[width=.7\textwidth]{img/daisy-example.png}
    % \end{figure}
  \end{frame}  

  \begin{frame}[fragile]{Library pydaisy}
    \begin{itemize}
      \item Can access and combine all levels of structured data      
      \item Enable access from Jupyter Notebook
      \item Test implementation with internal projects
    \end{itemize}
    \begin{figure}
      \includegraphics[width=.7\textwidth]{img/pydaisy-example-t.png}
    \end{figure}
  \end{frame}

  \begin{frame}{Unit Testing}

    \begin{minipage}{0.6\textwidth}
      \begin{itemize}
        \item Test a specific part of your program (unit) for correctness
        \item When adding new features, make sure all functionality keeps working 
        \item Test Driven Development (TDD)
        \begin{enumerate}
          \item Write \textcolor{red!80!black}{\textbf{Failing Test}} with new functionality
          \item Make test \textcolor{green!80!black}{\textbf{Succeed}} by changing your code
          \item \textcolor{blue!80!black}{\textbf{Clean Up}} your code and keep the tests passing
        \end{enumerate}
      \end{itemize} 
    \end{minipage}
    \hfill
    \begin{minipage}{0.35\textwidth}
      \begin{figure}[]
        \centering
          \includegraphics[height=.4\textheight]{img/tdd.png}
          % \includegraphics[height=.4\textheight]{img/go-tdd-t.png}
      \end{figure}
    \end{minipage}
  \end{frame}  

  \begin{frame}{Unit Testing}
    \begin{figure}[]
      \begin{tikzpicture}
        \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=.9\textwidth]{img/unittest-example.png}};
        \draw<2>[red,ultra thick,rounded corners] (4,0) rectangle (\textheight+2cm,2.1);
      \end{tikzpicture}      
    \end{figure}
  \end{frame}  

  \section{Electronic Health Record Analysis}

  \begin{frame}{Motivation}
    \begin{itemize}
      \item Direct Access to Martini-Klinik (UKE) data
      \begin{itemize}
        \item Prostate Cancer Clinic
        \item `With around  2400 prostate cancer operations per year [...] biggest Prostate Cancer Center [...] worldwide'\footnote{martini-klinik.de/en/for-patients/}
        \item Focus on (robot assisted) radical prostatectomy
      \end{itemize}
      \item We\footnote{Anne, Karin, Esther, Sumner, Patrick, Sven} want to
      \begin{itemize}
        \item Find disease/medication/treatment \textbf{correlations}
        \item Predict risks and \textbf{future progression}
        \item Assist in \textbf{clinical decision making}
        \item Gain Results that are interpretable, avoid `\textbf{black box}' problem
      \end{itemize}
    \end{itemize}
  \end{frame}

  \begin{frame}{Properties of an Electronic Health Record}
    \begin{description}
      \item[Incomplete] Missing information about \{treatments, medications, diagnoses, \dots\} from e.g. another hospital?\\Is patient absent because he is healthy or dead?
      \item[Heterogen] \textbf{Differences} in individual patients (disease/treatment/medication)
      \item[Sparse] Tracking in \textbf{inconsistent intervals} \\Was every clinical event properly recorded?
      \item[Bias] One patient will most likely get \textbf{different EHRs} from doctor to doctor or hospital to hospital
    \end{description}
  \end{frame}

  \begin{frame}{Goals - Questions}
    
    \begin{itemize}
      \item Build a full representation of individual patient's medical history 
      \item Get interpretable results  
      \item Predict
      \begin{itemize}
        \item Is a biopsy necessary? (Predict results)
        \item How long is a patient's progression-free survival?
        \item Side-effects of surgery
        \item Which treatment options should a doctor choose from?
        \item \dots
      \end{itemize}
    \end{itemize}
    
  \end{frame}  

  \begin{frame}{Goals - Potential Answers}
    \begin{minipage}{0.39\textwidth}
      What do we do with the data?
      \begin{itemize}
        \item Nomogram Evaluation
        \item Emulate Doctor's decisions
        \begin{itemize}
          \item Decision Support System
          \item Recommender System
        \end{itemize}
        \item Smart Recommendation
        \begin{itemize}
          \item Patient Clustering
        \end{itemize}
      \end{itemize}
    \end{minipage}
    \hfill
    \begin{minipage}{0.6\textwidth}
      \begin{figure}
        \includegraphics<2>[width=.9\textwidth]{img/nomogram.png}
      \end{figure}
    \end{minipage}
  \end{frame}   

  \begin{frame}{Status}
    \begin{itemize}
      \item Build Knowledge Base
      \begin{itemize}
        \item Nomograms
        \item Clinical Decision Support Systems
        \item Recommender System
        \item Patient Representation
      \end{itemize}
      \item Get the Data
      \begin{itemize}
        \item PLCO Dataset 
        \item Martini-Klinik
        \item \color{gray}{Pioneer}
      \end{itemize}
      \item Data Exploration
      \begin{itemize}
        \item Cluster Analysis 
      \end{itemize}
    \end{itemize}    
  \end{frame}  

  \subsection{PLCO}

  \begin{frame}{Overview}
    \begin{figure}[]
      \begin{tikzpicture}
        \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=.65\textwidth]{img/plco-schematic-t}};
        \draw<2>[red,ultra thick,rounded corners] (6.5,3.5) 
                                        rectangle (9,0);
      \end{tikzpicture}      
    \end{figure}    
    The \textbf{Prostate}, Lung, Colorectal and Ovarian (PLCO) Cancer Screening Trial is a large randomized trial [...] to determine the effects of screening on cancer-related mortality [...] in men and women aged 55 to 74.\footnote{prevention.cancer.gov/major-programs/prostate-lung-colorectal/}
  \end{frame}

  \begin{frame}{Tasks}
    \begin{itemize}
      \item Get understanding of individual and collective patient data
      \item Visual representations
      \item Run first analyses on our data pipeline
      \begin{itemize}
        \item Nomogram Evaluation
        \item Prediction (e.g. screening result $\rightarrow$ treatment)
      \end{itemize}
      \item EHR pipeline
    \end{itemize}
  \end{frame}      

  \begin{frame}{Data Exploration}
    \begin{itemize}
      \item Relevant Data: 
      \begin{itemize}
        \item 77k male
        \item 15.6k with diagnostic procedures
        \item 8.8k diagnosed with prostate cancer
        \item 3.2k radical prostatectomies
      \end{itemize}
      \item Data split into
      \begin{itemize}
        \item Screenings (digital rectal exam, PSA test, ...)
        \item Procedures 
        \item Complications (incontinence, diminished potency and blood loss)
        \item Treatments
      \end{itemize}
    \end{itemize}
  \end{frame}  

  \begin{frame}{Sample Patient Visualization}
    \includegraphics[width=\textwidth]{img/ehr_sample6-t}
  \end{frame}

  \begin{frame}{Sample Patient Visualization II}
    \begin{figure}[]
      \includegraphics[width=1\textwidth]{tikz/sample_patient2_2}
      \caption*{\color{MidnightBlue}{screening}, \color{Maroon}{diagnostic}, \color{Fuchsia}{treatment}, \color{ForestGreen}{complication}}
    \end{figure}
    
    \begin{itemize}
      \item Year joined and days from start to specific event (x-Axis)
    \end{itemize}
  \end{frame}  

  \begin{frame}{EHR Representation}    
    Possible Approach:
    \begin{itemize}
      \item A patient's EHR consists of events
      \item Can be clustered by e.g. visit
      \item Assign codes for \{diagnoses, medications, treatments, \dots\}
    \end{itemize}
    \vfill
    \begin{tikzpicture}
      \matrix [
        matrix of math nodes,
        ampersand replacement=\&,
        nodes={
          rectangle, 
          draw,
          minimum size=1cm,
          minimum width=1.2cm,
          % minimum height=0.7cm,
          anchor=center
          },
          ] (res)
          {
            D52\&T14\&T12\&M2\& {} \&D3\&M12\&T33\& T34 \& \phantom{ } \& \dots \& \\
          };
          \draw [decorate,decoration={brace,amplitude=10pt}] (-6.5,0.75) -- (-2,0.75) node [midway, yshift=20pt] 
          {\footnotesize{Visit}};
          \draw [decorate,decoration={brace,amplitude=5pt}] (-5.55,-0.75) -- (-6.5,-0.75) node [midway, yshift=-10pt] 
          {\footnotesize{Event}};
          \end{tikzpicture}    
    \vfill
  \end{frame}   

  \begin{frame}{Data Exploration II}
    Screenings
    \begin{itemize}
      \item Regular cancer screenings for half of the participants
      \item Relevant for us: Digital Rectal Exam, PSA Test
    \end{itemize}
    \begin{minipage}{.5\textwidth}
      Procedures
      \begin{itemize}
        \item Contains information for all patients
      \item Mostly screenings and imaging data
    \end{itemize}
    \end{minipage}
    \begin{minipage}{.45\textwidth}
      \scriptsize
      \begin{tikzpicture}[scale=0.5]
        \pie 
        {46/Repeat Screen,
           25/Imaging, 
           18/{Invasive Scopes, Biopsies \& Surgeries},
           11/Other};
        \end{tikzpicture}
    \end{minipage}
  \end{frame}  

  \begin{frame}{Data Exploration III}
    \begin{minipage}{.49\textwidth}
      \scriptsize
      \begin{tikzpicture}[scale=0.5]
        \pie 
        {31/Radiation,
           23/Prostatectomy, 
           22/{Hormone Therapy},
           21/Non-Curative,
           3/Other};
        \end{tikzpicture}
    \end{minipage}
    \begin{minipage}{.3\textwidth}
        \scriptsize
        \begin{tikzpicture}[scale=0.5]
          \pie 
          {62/Direct Surgical,
             14/Infection, 
             5/Cardiovascular or Pulmonary,
             20/Other};
        \end{tikzpicture}
        \begin{center}
        \end{center}
      \end{minipage}   
      \begin{center}
        Treatments\hspace{5cm} Complications
    \end{center}       
  \end{frame}  

  \subsection{Martini-Klinik}
  
  \begin{frame}{Martini-Klinik}
    \begin{minipage}{0.55\textwidth}
      \begin{itemize}
        \item Belongs to UKE
        \item Specialized on robotic assisted radical prostatectomy
        \item Data from 1992 until today
        \item High motivation to work with us
      \end{itemize}
    \end{minipage}
    \hfill
    \begin{minipage}{0.3\textwidth}
      \includegraphics[width=.8\textwidth]{img/Martini-Klinik_Logo.png}
      \vspace{4cm}
    \end{minipage}
  \end{frame} 

  \begin{frame}{Status}
    \begin{itemize}
      \item First look into data structure
      \item Meeting with lead statistician
      \item Get data presentation at Martini-Klinik
    \end{itemize}
  \end{frame}  


  \begin{frame}{Tasks}
    \begin{itemize}
      \item Transfer Data
      \item Integrate with PLCO data in pipeline
      \item Data exploration
    \end{itemize}
  \end{frame}    

  \subsection{Outlook}

  \begin{frame}{Status}
    \begin{itemize}
      \item Basic understanding of EHRs
      \item First data sources
      \item Started data exploration and visualization
    \end{itemize}
  \end{frame}  

  \begin{frame}{Next Steps}
    \begin{itemize}
      \item Transfer Data from Martini-Klinik
      \item Data Integration Pipeline
      \item Patient Representation and Visualization
      \item Run Analyses
      \begin{itemize}
        \item Nomograms
        \item Predictions
        \item \dots
      \end{itemize}
    \end{itemize}
  \end{frame}    
  
\end{document}